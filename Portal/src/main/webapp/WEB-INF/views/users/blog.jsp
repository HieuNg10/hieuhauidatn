<!-- sử dụng tiếng việt -->
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>

<!-- JSTL -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Store</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <jsp:include page="/WEB-INF/views/users/common/css.jsp"></jsp:include>

</head>

<body>
<%--header--%>
<jsp:include page="/WEB-INF/views/users/common/header.jsp"></jsp:include>

    <!-- Breadcrumb Section Begin -->
    <div class="breacrumb-section" style="margin-top: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-text product-more">
                        <a href="${base}/"><i class="fa fa-home"></i> Trang chủ</a>
                        <span>Tin tức</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Section Begin -->

<section class="blog-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-6 col-sm-8 order-2 order-lg-1">
                <div class="blog-sidebar">
                    <div class="search-form">
                        <h4>Tìm kiếm</h4>
                        <form action="#">
                            <input type="text" placeholder="Tìm kiếm . . .  ">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 order-1 order-lg-2">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="blog-item">
                            <div class="bi-pic">
                                <img src="img/blog/blog1.jpg" alt="">
                            </div>
                            <div class="bi-text">
                                <a href="./blog-details.html">
                                    <h4>Sofa nhập khẩu Ý da bò thật Apollo</h4>
                                </a>
                                <p>Sofa <span>- 24/08/2023</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="blog-item">
                            <div class="bi-pic">
                                <img src="img/blog/blog2.jpg" alt="">
                            </div>
                            <div class="bi-text">
                                <a href="./blog-details.html">
                                    <h4>Kệ tivi để sàn deco</h4>
                                </a>
                                <p>Tivi <span>- 20/08/2023</span></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-sm-6">
                        <div class="blog-item">
                            <div class="bi-pic">
                                <img src="img/blog/blog4.jpg" alt="">
                            </div>
                            <div class="bi-text">
                                <a href="./blog-details.html">
                                    <h4>Bàn làm việc hiện đại</h4>
                                </a>
                                <p>Table <span>- 18/08/2023</span></p>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-6 col-sm-6">
                        <div class="blog-item">
                            <div class="bi-pic">
                                <img src="img/blog/blog3.jpg" alt="">
                            </div>
                            <div class="bi-text">
                                <a href="./blog-details.html">
                                    <h4>Kệ trang trí tranh đồng hồ</h4>
                                </a>
                                <p>Deco <span>- 18/08/2023</span></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="flex-c-m flex-w w-full p-t-45">
                            <a href="#" class="flex-c-m stext-101 cl5 size-103 bg2 bor1 hov-btn1 p-lr-15 trans-04">
                                Xem thêm
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    <!-- Footer Section Begin -->
    <jsp:include page="/WEB-INF/views/users/common/footer.jsp"></jsp:include>
    <!-- Footer Section End -->

    <!-- java script -->
    <jsp:include page="/WEB-INF/views/users/common/js.jsp"></jsp:include>
</body>

</html>
