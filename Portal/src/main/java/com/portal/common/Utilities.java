package com.portal.common;

import com.github.slugify.Slugify;

public class Utilities {
	
	public static String createSeoLink(final String text) {
		Slugify slg = new Slugify();
		String result = slg.slugify(text + "-" + System.currentTimeMillis());
		return result;
	}
	
	public static void main(String[] args) {
		System.out.println(createSeoLink(" Điện Thoại Vsmart Joy3 (2G+32G) Đen đại dương "));
	}
	
}
